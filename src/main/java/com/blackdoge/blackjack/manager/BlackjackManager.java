package com.blackdoge.blackjack.manager;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.blackdoge.blackjack.engine.ComputerDealer;
import com.blackdoge.blackjack.engine.Game;
import com.blackdoge.blackjack.model.Hand;
import com.blackdoge.blackjack.model.Player.Action;
import com.blackdoge.blackjack.model.rest.GameInfo;

public class BlackjackManager {
    private static BlackjackManager instance;
    private Map<String, Game> games;

    private static final float MAX_BET = 500.0f;
    private static final float MIN_BET = 0.0f;

    private BlackjackManager() {
        games = new HashMap<String, Game>();
    }

    public static BlackjackManager getInstance() {
        if (instance == null) {
            instance = new BlackjackManager();
        }
        return instance;
    }

    public synchronized GameInfo newGame(String accountId, float bet) {
        if (!StringUtils.isEmpty(accountId) && bet >= MIN_BET && bet <= MAX_BET) {
            Game newInstance = new Game(bet);
            games.put(accountId, newInstance);
            if(newInstance.assessBlackjack() == Hand.Result.PlayerBlackjack) {
                newInstance.getPlayer().actionsPending = false;
            }
            GameInfo gameInfo = newInstance.getGameInfo();

            return gameInfo;
        }
        return null;
    }

    public GameInfo getGameInfo(String accountId) {
        if (!StringUtils.isEmpty(accountId)) {
            return games.get(accountId).getGameInfo();
        }
        return null;
    }

    public synchronized GameInfo act(String accountId, Action action) {
        if (!StringUtils.isEmpty(accountId) && action != null) {
            Game game = games.get(accountId);
            if (game != null) {
                game.playerAct(action);
                if (game.getPlayer().actionsPending == false) {
                    while (game.getDealer().actionsPending == true) {
                        game.dealerAct(ComputerDealer.calculateAction(game.getDealer().getHand()));
                    }
                }
                GameInfo toReturn = game.getGameInfo();
                return toReturn;
            }
            
        }
        return null;
    }
}
