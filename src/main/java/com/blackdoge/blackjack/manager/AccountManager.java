package com.blackdoge.blackjack.manager;

import com.blackdoge.blackjack.rest.model.Account;
import com.blackdoge.hibernate.utils.HibernateUtil;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;

public class AccountManager {

    private static AccountManager instance;
    private static Session dbSession;
    
    private AccountManager() {
        dbSession = HibernateUtil.getSessionFactory().openSession();
    }
    
    @Override
    public void finalize() throws Throwable {
        super.finalize();
        dbSession.close();
    }

    public static AccountManager getInstance() {
        if (instance == null) {
            instance = new AccountManager();
        }
        return instance;
    }

    public synchronized Account createAccount() {
        Account newAccount = new Account();
        dbSession.save(newAccount);
        dbSession.flush();
        return newAccount;
    }

    public Account getAccount(String accountId) {
        if (!StringUtils.isEmpty(accountId)) {
            return (Account) dbSession.get(Account.class, accountId);
        }
        return null;
    }
}
