package com.blackdoge.blackjack.rest.model;

import java.io.Serializable;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Account")
public class Account implements Serializable {

    private String id;
    private String dogecoinAddress;
    private float balance;

    public Account() {
        id = UUID.randomUUID().toString();
        balance = 0.0f;
    }

    @Id
    @Column(name = "id", unique = true, nullable = false)
    public String getId() {
        return id;
    }
    
    public void setId(String id) {
        this.id = id;
    }

    @Column(name = "dogecoin_address", unique = true, nullable = false)
    public String getDogecoinAddress() {
        return dogecoinAddress;
    }
    
    public void setDogecoinAddress(String address) {
        this.dogecoinAddress = address;
    }

    public float getBalance() {
        return balance;
    }

    public void setBalance(float balance) {
        this.balance = balance;
    }
}
