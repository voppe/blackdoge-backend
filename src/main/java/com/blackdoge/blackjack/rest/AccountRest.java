package com.blackdoge.blackjack.rest;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.commons.lang.StringUtils;

import com.blackdoge.blackjack.manager.AccountManager;
import com.blackdoge.blackjack.rest.model.Account;

@Path("/account")
public class AccountRest {
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Account getAccount(@QueryParam("id") String accountId) {
        if (!StringUtils.isEmpty(accountId)) {
            return AccountManager.getInstance().getAccount(accountId);
        }
        return null;
    }
    
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Account newAccount() {
        return AccountManager.getInstance().createAccount();
    }
}
