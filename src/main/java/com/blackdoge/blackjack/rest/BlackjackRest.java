package com.blackdoge.blackjack.rest;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.commons.lang.StringUtils;

import com.blackdoge.blackjack.manager.BlackjackManager;
import com.blackdoge.blackjack.manager.AccountManager;
import com.blackdoge.blackjack.model.Player.Action;
import com.blackdoge.blackjack.model.rest.GameInfo;
import com.blackdoge.blackjack.rest.model.Account;
import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.core.MultivaluedMap;
import org.apache.log4j.Logger;

@Path("/blackjack")
public class BlackjackRest {    
    private static final Logger log = Logger.getLogger(BlackjackRest.class);
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public GameInfo getGameInfo(@QueryParam("id") String accountId) {
        if (!StringUtils.isEmpty(accountId)) {
            return BlackjackManager.getInstance().getGameInfo(accountId);
        }
        return null;
    }

    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public GameInfo newGame(MultivaluedMap<String, String> params) {
        String accountId = "";
        String betAmount = "";
        try {
            accountId = params.get("id").get(0);
            betAmount = params.get("bet").get(0);
        } catch(NullPointerException e) {
            // Invalid request
        }
        
        log.info("Received new game request for accountId \""+accountId+"\" for "+betAmount);
        if (!StringUtils.isEmpty(accountId) && !StringUtils.isEmpty(betAmount)) {
            Account account = AccountManager.getInstance().getAccount(accountId);
            if (account != null) {
                float bet = -1;
                try {
                    bet = Float.parseFloat(betAmount);
                } catch (NumberFormatException e) {
                }
//                if (account.getBalance() >= bet) {
                    account.setBalance(account.getBalance() - bet);
                    GameInfo newGame = BlackjackManager.getInstance().newGame(accountId, bet);
                    return newGame;
//                } // ELSE ERROR
            } // ELSE ERROR
        }
        return null;
    }

    @PUT
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public GameInfo takeAction(MultivaluedMap<String, String> params) {
        String accountId = "";
        String actionName = "";
        try {
            accountId = params.get("id").get(0);
            actionName = params.get("action").get(0);
        } catch(NullPointerException e) {
            // Invalid request
        }
        
        Action action = null;
        if (!StringUtils.isEmpty(accountId) && !StringUtils.isEmpty(actionName)) {
            String actionSwitch = actionName.trim().toUpperCase();
            if ("HIT".equals(actionSwitch)) {
                action = Action.Hit;
            } else if ("STAY".equals(actionSwitch)) {
                action = Action.Stay;
            }

        }
        if (action != null) {
            return BlackjackManager.getInstance().act(accountId, action);
        }
        return null;
    }
}
