/**
 * Project: BasicBlackJack
 * Package: com.gbayer.basicblackjack
 * File: Player.java
 * Author: Greg Bayer <greg@gbayer.com>
 * Date: Jul 19, 2010
 */
package com.blackdoge.blackjack.model;

/**
 * A <code>Player</code> has a name assigned, holds a <code>Hand</code> of
 * <code>Cards</code>, and is capable of taking actions.
 */
public class Player
{
	/** The player's current hand. */
	protected Hand hand;

	/** Whether the player can take action or not */
	public boolean actionsPending;
	
	/**
	 * Action a player can take (or be forced to take).
	 */
	public enum Action
	{
		Hit, Stay, Bust
	}

	/**
	 * Instantiates a new player.
	 * 
	 * @param name
	 *            the name
	 */
	public Player()
	{
		hand = new Hand();
                actionsPending = true;
	}

	/**
	 * Gets the player's hand.
	 * 
	 * @return the hand
	 */
	public Hand getHand()
	{
		return hand;
	}

}
