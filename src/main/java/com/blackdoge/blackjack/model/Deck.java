/**
 * Project: BasicBlackJack Package: com.gbayer.basicblackjack File: Deck.java
 * Author: Greg Bayer <greg@gbayer.com>
 * Date: Jul 19, 2010
 */
package com.blackdoge.blackjack.model;

import java.util.Collections;
import java.util.Stack;

import org.apache.log4j.Logger;

/**
 * A <code>Deck</code> of <code>Cards</code> in play for the current
 * <code>Game</code>.
 */
public class Deck {
    /**
     * The Log4J logger.
     */
    private static Logger log = Logger.getLogger(Deck.class);
/**
     * Constant - Value of any face card.
     */
    public static final int FACE_CARD_VALUE = 10;

    /**
     * Constant - Low value of an ace.
     */
    public static final int LOW_ACE_VALUE = 1;

    /**
     * Constant - High value of an ace.
     */
    public static final int HIGH_ACE_VALUE = 11;

    /**
     * Constant - Card id representing an ace.
     */
    public static final int ACE_ID = 1;

    /**
     * Constant - Card id representing an jack.
     */
    public static final int JACK_ID = 11;

    /**
     * Constant - Card id representing an queen.
     */
    public static final int QUEEN_ID = 12;

    /**
     * Constant - Card id representing an king.
     */
    public static final int KING_ID = 13;

    /**
     * Constant - All cards with ids equal or greater are face cards.
     */
    public static final int FIRST_FACE_CARD_ID = JACK_ID;

    /**
     * Constant - Number of unique card ids.
     */
    public static final int MAX_CARD_ID = 13;
    
    /**
     * The cards.
     */
    private Stack<Card> cards;

    /**
     * Instantiates a new deck.
     *
     */
    public Deck(int howMany) {
        cards = new Stack<Card>();
        newDeck();
    }

    public Deck() {
        this(1);
    }

    /**
     * Clear existing deck and generate/shuffle a new one. Reuses underlying
     * data structure.
     */
    public void newDeck() {
        log.info("Generating new deck");

        cards.clear();

        for (int i = 0; i < 4; i++) {
            Suit suit = null;
            if (i == 0) {
                suit = Suit.CLUBS;
            } else if (i == 1) {
                suit = Suit.DIAMONDS;
            } else if (i == 2) {
                suit = Suit.HEARTS;
            } else if (i == 3) {
                suit = Suit.SPADES;
            }
            for (int d = 0; d < MAX_CARD_ID; d++) {
                // Card values are from 1-13
                // Create 4 copies of each card value (one for each suit)

                cards.push(new Card(1 + d, suit));
            }
        }

        Collections.shuffle(cards);

        log.debug("Deck size is: " + cards.size());
    }

    /**
     * Removes a card off the top of the deck.
     *
     * @return the card
     */
    public Card removeCard() {
        return cards.pop();
    }

    /**
     * Count number of cards remaining.
     *
     * @return the number of cards remaining.
     */
    public int numCardsRemaining() {
        return cards.size();
    }
}
