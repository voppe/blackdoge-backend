/**
 * Project: BasicBlackJack Package: com.gbayer.basicblackjack File: Card.java
 * Author: Greg Bayer <greg@gbayer.com>
 * Date: Jul 19, 2010
 */
package com.blackdoge.blackjack.model;

/**
 * A Card within a <code>Deck</code> or a <code>Hand</code>
 */
public class Card {

    private int cardValue;
    private Suit suit;

    private Card() {
    }
    
    public Card(int cardValue, Suit suit) {
        this.cardValue = cardValue;
        this.suit = suit;
    }

    public int getCardValue() {
        return cardValue;
    }

    public void setCardValue(int cardValue) {
        this.cardValue = cardValue;
    }

    public Suit getSuit() {
        return suit;
    }

    public void setSuit(Suit suit) {
        this.suit = suit;
    }
}
