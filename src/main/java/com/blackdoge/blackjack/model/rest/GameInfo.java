package com.blackdoge.blackjack.model.rest;

import com.blackdoge.blackjack.model.Hand;

public class GameInfo {
    private PlayerInfo player;
    private PlayerInfo dealer;
    private float bet;
    private Hand.Result result;

    public GameInfo() {
    }

    public PlayerInfo getDealer() {
        return dealer;
    }

    public void setDealer(PlayerInfo dealerInfo) {
        this.dealer = dealerInfo;
    }

    public PlayerInfo getPlayer() {
        return player;
    }

    public void setPlayer(PlayerInfo playerInfo) {
        this.player = playerInfo;
    }

    public float getBet() {
        return bet;
    }

    public void setBet(float bet) {
        this.bet = bet;
    }

    public Hand.Result getResult() {
        return result;
    }

    public void setResult(Hand.Result result) {
        this.result = result;
    }    
}
