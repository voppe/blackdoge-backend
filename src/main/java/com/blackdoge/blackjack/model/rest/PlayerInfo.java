package com.blackdoge.blackjack.model.rest;

import com.blackdoge.blackjack.model.Card;
import java.util.List;

public class PlayerInfo {
    private List<Card> hand;
    private int handTotal;

    public PlayerInfo() {
    }

    public List<Card> getHand() {
        return hand;
    }

    public void setHand(List<Card> hand) {
        this.hand = hand;
    }

    public int getHandTotal() {
        return handTotal;
    }

    public void setHandTotal(int handTotal) {
        this.handTotal = handTotal;
    }

}
