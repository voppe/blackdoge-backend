/**
 * Project: BasicBlackJack Package: com.gbayer.basicblackjack File: Game.java
 * Author: Greg Bayer <greg@gbayer.com>
 * Date: Jul 19, 2010
 */
package com.blackdoge.blackjack.engine;

import com.blackdoge.blackjack.model.Card;
import com.blackdoge.blackjack.model.Deck;
import com.blackdoge.blackjack.model.Hand;
import org.apache.log4j.Logger;

import com.blackdoge.blackjack.model.Player;
import com.blackdoge.blackjack.model.Player.Action;
import com.blackdoge.blackjack.model.rest.GameInfo;
import com.blackdoge.blackjack.model.rest.PlayerInfo;
import java.util.ArrayList;
import java.util.List;

/**
 * The driver class for the BasicBackJack game.
 */
public class Game {

    /**
     * The Log4J logger.
     */
    private static Logger log = Logger.getLogger(Game.class);

    /**
     * The computer controlled dealer.
     */
    private Player dealer;

    /**
     * The human controlled player.
     */
    private Player player;

    /**
     * The bet amount
     */
    private float bet;

    /**
     * The deck in use for this game.
     */
    private Deck deck;

    /**
     * Instantiates a new game.
     *
     */
    public Game(float bet) {
        deck = new Deck();
        dealer = new Player();
        player = new Player();
        dealCards();
    }

    /**
     * Deal cards around the table (to player and dealer).
     */
    private void dealCards() {
        log.info("Dealing cards");

        // First card
        player.getHand().addCard(deck.removeCard());
        dealer.getHand().addCard(deck.removeCard());

        // Second card
        player.getHand().addCard(deck.removeCard());
        dealer.getHand().addCard(deck.removeCard());
    }

    /**
     * Process player actions. Update hand accordingly.
     *
     */
    public int playerAct(Action action) {
        return act(player, action);
    }

    public int dealerAct(Action action) {
        return act(dealer, action);
    }

    private int act(Player player, Action action) {
        if (player.actionsPending) {
            switch (action) {
                case Hit:
                    player.getHand().addCard(deck.removeCard());
                    break;
                case Stay:
                    player.actionsPending = false;
                    break;
                default:
            }

            int total = player.getHand().getTotalHandValue();
            if (total > Hand.MAX_HAND_VALUE) {
                log.info("Player busts.");

                player.actionsPending = false;
            }
        }
        return player.getHand().getTotalHandValue();
    }

    /**
     * Assess hand winner.
     *
     * @param playerOutcome the player outcome
     * @param dealerOutcome the dealer outcome
     * @return the hand. result
     */
    public Hand.Result assessHandWinner() {
        int playerOutcome = player.getHand().getTotalHandValue();
        int dealerOutcome = dealer.getHand().getTotalHandValue();
        if (playerOutcome > Hand.MAX_HAND_VALUE
                && dealerOutcome > Hand.MAX_HAND_VALUE) {
            return Hand.Result.Push;
        }
        if (playerOutcome > Hand.MAX_HAND_VALUE) {
            return Hand.Result.DealerWins;
        }
        if (dealerOutcome > Hand.MAX_HAND_VALUE) {
            return Hand.Result.PlayerWins;
        }

        if (playerOutcome > dealerOutcome) {
            return Hand.Result.PlayerWins;
        } else if (playerOutcome < dealerOutcome) {
            return Hand.Result.DealerWins;
        }
        
        Hand.Result blackjack = assessBlackjack();
        if(blackjack != null) {
            return blackjack;
        } else {
            return Hand.Result.Push;
        }
    }
    
    public Hand.Result assessBlackjack() {
        int playerOutcome = player.getHand().getTotalHandValue();
        int dealerOutcome = dealer.getHand().getTotalHandValue();
        
        boolean playerBlackjack = false;
        boolean dealerBlackjack = false;
        if (playerOutcome == Hand.MAX_HAND_VALUE && player.getHand().getCards().size() == 2 ) {
            playerBlackjack = true;
        }
        if (dealerOutcome == Hand.MAX_HAND_VALUE && dealer.getHand().getCards().size() == 2 ) {
            dealerBlackjack = true;
        }
        if(playerBlackjack != dealerBlackjack) {
            if(playerBlackjack == true) {
                return Hand.Result.PlayerBlackjack;
            }
            if(dealerBlackjack == true) {
                return Hand.Result.DealerBlackjack;
            }
        }
        return null;
    }

    /**
     * Gets the deck.
     *
     * @return the deck
     */
    public Deck getDeck() {
        return deck;
    }

    /**
     * Gets the GameInfo object.
     *
     * @return the deck
     */
    public GameInfo getGameInfo() {
        GameInfo gameInfo = new GameInfo();
        gameInfo.setBet(bet);

        PlayerInfo playerInfo = new PlayerInfo();
        playerInfo.setHand(player.getHand().getCards());
        playerInfo.setHandTotal(player.getHand().getTotalHandValue());
        gameInfo.setPlayer(playerInfo);

        // Get dealer cards
        PlayerInfo dealerInfo = new PlayerInfo();
        List<Card> cards = new ArrayList<Card>(dealer.getHand().getCards());
        if (player.actionsPending == true) {
            // Obscure first dealer card
            cards.set(0, new Card(0, null));
            dealerInfo.setHandTotal(cards.get(1).getCardValue());
        } else {
            dealerInfo.setHandTotal(dealer.getHand().getTotalHandValue());
            gameInfo.setResult(assessHandWinner());
        }
        dealerInfo.setHand(cards);
        gameInfo.setDealer(dealerInfo);
        return gameInfo;
    }

    public Player getDealer() {
        return dealer;
    }

    public Player getPlayer() {
        return player;
    }

}
